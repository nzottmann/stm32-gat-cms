/*
 * modbus.c
 *
 *  Created on: 14.05.2018
 *      Author: N
 */

#include "modbus.h"

void modbusRead(UCHAR * destination, USHORT * regBuffer, USHORT regIndex, USHORT nRegs, BOOL byteSwap) {
  while (nRegs > 0) {
    if(byteSwap) {
      *destination++ = (UCHAR) (regBuffer[regIndex] >> 8);
      *destination++ = (UCHAR) (regBuffer[regIndex] & 0xFF);
    }
    else {
      *destination++ = (UCHAR) (regBuffer[regIndex] & 0xFF);
      *destination++ = (UCHAR) (regBuffer[regIndex] >> 8);
    }
    regIndex++;
    nRegs--;
  }
}

void modbusWrite(UCHAR * source, USHORT * regBuffer, USHORT regIndex, USHORT nRegs, BOOL byteSwap) {
  while (nRegs > 0) {
    if(byteSwap) {
      regBuffer[regIndex] = ((USHORT)(*source++))<<8;
      regBuffer[regIndex] |= *source++;
    }
    else {
      regBuffer[regIndex] = *source++;
      regBuffer[regIndex] |= ((USHORT)(*source++))<<8;
    }
    regIndex++;
    nRegs--;
  }
}

void modbusControl_callback(void) {
  if(g_modbusData.control[MB_REG_CONTROL_RECCONTROL] == 1) {
    g_modbusData.control[MB_REG_CONTROL_RECCONTROL] = 0;
    if(g_recState == STATE_REC_STOPPED) {
      if(g_sdPresent) g_recState = STATE_REC_STARTING;
    }
  }
  else if(g_modbusData.control[MB_REG_CONTROL_RECCONTROL] == 0) {
    if(g_recState == STATE_REC_RUNNING) g_recState = STATE_REC_STOPPING;
  }
}


eMBErrorCode eMBRegInputCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs )
{
  return MB_ENOREG;
}


eMBErrorCode eMBRegHoldingCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode )
{
  /* usAddress is incremented internally before */
  usAddress--;

  modbusMemory_t memoryScopesRegister[] = {
      {.regStart = MB_REG_SENSOR_START, .nRegs = MB_REG_SENSOR_NREGS, .buffer = (USHORT *)g_modbusData.sensor, .byteSwap = TRUE, .readCallback = NULL, .writeCallback = NULL},
      {.regStart = MB_REG_THR1_START, .nRegs = MB_REG_THR1_NREGS, .buffer = (USHORT *)g_modbusData.thr1, .byteSwap = TRUE, .readCallback = NULL, .writeCallback = NULL},
      {.regStart = MB_REG_THR2_START, .nRegs = MB_REG_THR2_NREGS, .buffer = (USHORT *)g_modbusData.thr2, .byteSwap = TRUE, .readCallback = NULL, .writeCallback = NULL},
      {.regStart = MB_REG_THR3_START, .nRegs = MB_REG_THR3_NREGS, .buffer = (USHORT *)g_modbusData.thr3, .byteSwap = TRUE, .readCallback = NULL, .writeCallback = NULL},
      {.regStart = MB_REG_CM_START, .nRegs = MB_REG_CM_NREGS, .buffer = (USHORT *)g_modbusData.cmParam, .byteSwap = TRUE, .readCallback = NULL, .writeCallback = NULL},
      {.regStart = MB_REG_THRCNT1_START, .nRegs = MB_REG_THRCNT1_NREGS, .buffer = (USHORT *)g_modbusData.thrCnt1, .byteSwap = TRUE, .readCallback = NULL, .writeCallback = NULL},
      {.regStart = MB_REG_THRCNT2_START, .nRegs = MB_REG_THRCNT2_NREGS, .buffer = (USHORT *)g_modbusData.thrCnt2, .byteSwap = TRUE, .readCallback = NULL, .writeCallback = NULL},
      {.regStart = MB_REG_THRCNT3_START, .nRegs = MB_REG_THRCNT3_NREGS, .buffer = (USHORT *)g_modbusData.thrCnt3, .byteSwap = TRUE, .readCallback = NULL, .writeCallback = NULL},
      {.regStart = MB_REG_CUSTOMER_START, .nRegs = MB_REG_CUSTOMER_NREGS, .buffer = (USHORT *)g_modbusData.customerData, .byteSwap = TRUE, .readCallback = NULL, .writeCallback = NULL},
      {.regStart = MB_REG_CONTROL_START, .nRegs = MB_REG_CONTROL_NREGS, .buffer = (USHORT *)g_modbusData.control, .byteSwap = TRUE, .readCallback = NULL, .writeCallback = modbusControl_callback},
      {.regStart = MB_REG_INFO_START, .nRegs = MB_REG_INFO_NREGS, .buffer = (USHORT *)g_modbusData.cmsSerialNumber, .byteSwap = FALSE, .readCallback = NULL, .writeCallback = NULL},
      {.regStart = MB_REG_INFONUMERIC_START, .nRegs = MB_REG_INFONUMERIC_NREGS, .buffer = (USHORT *)(&(g_modbusData.vendorDeliveryDate)), .byteSwap = TRUE, .readCallback = NULL, .writeCallback = NULL}
  };

  for(int s = 0; s < sizeof(memoryScopesRegister)/sizeof(memoryScopesRegister[0]); s++) {
    if(MB_INRANGE(usAddress, usNRegs, memoryScopesRegister[s].regStart, memoryScopesRegister[s].nRegs)) {
      if(eMode == MB_REG_READ) {
        modbusRead(pucRegBuffer, memoryScopesRegister[s].buffer, usAddress - memoryScopesRegister[s].regStart, usNRegs, memoryScopesRegister[s].byteSwap);
        if(memoryScopesRegister[s].readCallback) memoryScopesRegister[s].readCallback();
      }
      else if(eMode == MB_REG_WRITE) {
        modbusWrite(pucRegBuffer, memoryScopesRegister[s].buffer, usAddress - memoryScopesRegister[s].regStart, usNRegs, memoryScopesRegister[s].byteSwap);
        if(memoryScopesRegister[s].writeCallback) memoryScopesRegister[s].writeCallback();
      }

      return MB_ENOERR;
    }
  }

  /* Info registers */
  /*if(MB_INRANGE(usAddress, usNRegs, MB_REG_INFO_START, MB_REG_INFO_NREGS)) {
    USHORT iRegIndex = usAddress - MB_REG_INFO_START;
    USHORT *iRegBuf = (USHORT *)(&(g_deviceInfo));
    while (usNRegs > 0) {
      *pucRegBuffer++ = (UCHAR) (iRegBuf[iRegIndex] & 0xFF);
      *pucRegBuffer++ = (UCHAR) (iRegBuf[iRegIndex] >> 8);
      iRegIndex++;
      usNRegs--;
    }
    return MB_ENOERR;
  }*/

  return MB_ENOREG;
}


eMBErrorCode eMBRegCoilsCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils, eMBRegisterMode eMode )
{
    return MB_ENOREG;
}

eMBErrorCode eMBRegDiscreteCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete )
{
    return MB_ENOREG;
}
