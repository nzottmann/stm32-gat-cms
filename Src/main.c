/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "cmsis_os.h"
#include "adc.h"
#include "dma.h"
#include "fatfs.h"
#include "opamp.h"
#include "rtc.h"
#include "sdmmc.h"
#include "tim.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "usbd_core.h" // USBD_Start, Stop
#include <math.h> //sqrt, pow
#include <errno.h>
#include <sys/unistd.h> // STDOUT_FILENO, STDERR_FILENO
#include "ini.h"
#include "modbus.h"
#include "mb.h"
#include "mbport.h"
#include "gitdescribe.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

extern char SD_Path[];
extern osThreadId vibrationTaskHandle;
extern osThreadId sdTaskHandle;
extern osMessageQId modbusQueueHandle;
extern const char gitDescribe[];

/* Modbus port */
TIM_HandleTypeDef *mbhtim = &htim7;
UART_HandleTypeDef *mbhuart = &huart1;
osMessageQId *mbEventQueueHandle = &modbusQueueHandle;

uint32_t g_sdPresent = 0;
float g_visuData[2] = {0.0, 0.0};

uint16_t adc2DmaBuffer[ADC2BUFSIZE];
float rmsBuffer[RMSBUFSIZE];

enum adcChannel {
  MEMS_X = 0,
  MEMS_Y = 1,
  MEMS_Z = 2,
  PZO = 3,
  PZO_EXT = 4,
  RTD = 5
};

typedef struct {
  float hardSens; // LSB / sensor output (V, Ohm, ...)
  float hardOffset; // V
  float softSens; // V / sensed unit (g, �C)
  float softOffset; // sensed unit (g, �C)
} adcCal_t;

typedef struct {
  adcCal_t *adcCal;
  uint32_t enableLog;
  uint32_t nLogChannels;
  uint32_t dev;
  uint32_t logChannelOrder[ADC2SAMPLECHCNT];
  RTC_DateTypeDef date;
  RTC_TimeTypeDef time;
} globalConfig_t;

adcCal_t adcCal[ADC2SAMPLECHCNT] = {
  {.hardSens = LSBPERVOLT, .hardOffset = 0.9, .softSens = 0.08f, .softOffset = 0}, // MEMS_X
  {.hardSens = LSBPERVOLT, .hardOffset = 0.9, .softSens = 0.08f, .softOffset = 0}, // MEMS_Y
  {.hardSens = LSBPERVOLT * MEMS_AMP, .hardOffset = 0.9, .softSens = 0.08f, .softOffset = 0}, // MEMS_Z
  {.hardSens = -1.0f*LSBPERVOLT, .hardOffset = -1.5, .softSens = 0.1f, .softOffset = 0}, // PZO
  {.hardSens = 1.0f*LSBPERVOLT, .hardOffset = 1.5, .softSens = 0.1f, .softOffset = 0}, // PZO_EXT
  {.hardSens = LSBPERVOLT * 0.0179f /* LSB/V * Ohm/LSB = LSB/Ohm */ * 0.95376 /* Compensate voltage drop on 180k output series resistor */, .hardOffset = 0, .softSens = 0.388f, .softOffset = 258.0f} // RTD
};

globalConfig_t g_config = {
  .adcCal = adcCal,
  .enableLog = 1,
  .nLogChannels = ADC2DEFAULTLOGCHCNT,
  .dev = 0,
  .logChannelOrder = {MEMS_Z, PZO, RTD, MEMS_X, MEMS_Y, PZO_EXT},
  .date = {.Year = 17, .Month = 6-1, .Date = 1, .WeekDay = RTC_WEEKDAY_MONDAY},
  .time = {.Hours = 0, .Minutes = 0, .Seconds = 0, .SubSeconds = 0, .DayLightSaving = RTC_DAYLIGHTSAVING_NONE, .StoreOperation = RTC_STOREOPERATION_RESET}
};

enum recordingState g_recState = STATE_REC_STOPPED;

typedef struct {
  uint32_t overflows;
  uint16_t data[ADC2BUFSIZE/2];
} rawAdcData_t;

osMailQDef (q_rawAdcData, 21, rawAdcData_t);  // Declare mail queue
osMailQId  (q_rawAdcDataHandle);                 // Mail queue ID
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
int _write(int file, char *data, int len);
FRESULT diskWriteTest();
int writeSdInfo();
int readConfiguration();
void writeUid();
int ini_parse_fatfs(const char* filename, ini_handler handler, void* user);

void StartVibrationTask(void const * argument);
void StartSdTask(void const * argument);
void StartBtTask(void const * argument);
void StartThresholdTask(void const * argument);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC2_Init();
  MX_OPAMP1_Init();
  MX_OPAMP2_Init();
  MX_TIM4_Init();
  MX_USART1_UART_Init();
  MX_RTC_Init();
  MX_TIM7_Init();

  /* USER CODE BEGIN 2 */

  /* LED test sequence */
  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
  HAL_Delay(200);
  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(LED_Y_GPIO_Port, LED_Y_Pin, GPIO_PIN_SET);
  HAL_Delay(200);
  HAL_GPIO_WritePin(LED_Y_GPIO_Port, LED_Y_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_SET);
  HAL_Delay(200);
  HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);

  /* Power LED */
  HAL_GPIO_WritePin(LED_0_GPIO_Port, LED_0_Pin, GPIO_PIN_SET);

  q_rawAdcDataHandle = osMailCreate(osMailQ(q_rawAdcData), NULL);
  /* USER CODE END 2 */

  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();

  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 20;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USART1
                              |RCC_PERIPHCLK_USB|RCC_PERIPHCLK_SDMMC1
                              |RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLLSAI1;
  PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 12;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK|RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_RCC_MCOConfig(RCC_MCO1, RCC_MCO1SOURCE_SYSCLK, RCC_MCODIV_1);

    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/* USER CODE BEGIN 4 */
void StartVibrationTask(void const * argument)
{
  osEvent evt;
  uint16_t *curAdcBuf;
  rawAdcData_t *pd;
  uint32_t overflows = 0;

  /* Configure sensors */
  MEMS_RANGE_10G();
  MEMS_STANDBY_DISABLE();
  //HAL_GPIO_WritePin(MEMS_ST1_GPIO_Port, MEMS_ST1_Pin, GPIO_PIN_SET);
  //HAL_GPIO_WritePin(MEMS_ST2_GPIO_Port, MEMS_ST2_Pin, GPIO_PIN_SET);

  HAL_OPAMP_Start(&hopamp1);
  HAL_OPAMP_Start(&hopamp2);

  /* Calibrate ADC before usage */
  //HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED);
  HAL_ADCEx_Calibration_Start(&hadc2, ADC_SINGLE_ENDED);

  //HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adc1DmaBuffer, ADC1BUFSIZE);
  HAL_ADC_Start_DMA(&hadc2, (uint32_t*)adc2DmaBuffer, ADC2BUFSIZE);

  /* ADC is triggered by timer */
  HAL_TIM_Base_Start(&htim4);

  /* This task has high priority. Sleep until SD is ready */
  do {
    evt = osSignalWait(SIG_SDREADY, osWaitForever);
  } while(!(evt.value.signals & SIG_SDREADY));

  /* Dismiss first samples, they are invalid */
  osSignalWait(SIG_ADC2HALF, osWaitForever);
  osSignalWait(SIG_ADC2CPLT, osWaitForever);

  while(1) {
    evt = osSignalWait(SIG_ADC2HALF | SIG_ADC2CPLT, osWaitForever);
    if(evt.status != osEventSignal) continue;

    /* ADC2 */
    if(evt.value.signals & (SIG_ADC2HALF | SIG_ADC2CPLT)) {
      curAdcBuf = (evt.value.signals & SIG_ADC2HALF) ? adc2DmaBuffer : adc2DmaBuffer + ADC2BUFSIZE/2;

      pd = (rawAdcData_t *) osMailAlloc(q_rawAdcDataHandle, 0);
      if(pd == NULL) {
        /* malloc failed, queue is full */
        overflows++;
      }
      else {
        /* Copy adcBuf to mail queue */
        HAL_DMA_Start(&hdma_memtomem_dma1_channel3, (uint32_t)curAdcBuf, (uint32_t)(pd->data), ADC2BUFSIZE/2*sizeof(uint16_t) /4); // DMA transfers words -> /4
        pd->overflows = overflows;

        if(HAL_DMA_PollForTransfer(&hdma_memtomem_dma1_channel3, HAL_DMA_FULL_TRANSFER, 10) != HAL_OK) {
          Error_Handler();
        }
        osMailPut(q_rawAdcDataHandle, pd);
      }
    }
  }
}

void StartSdTask(void const * argument)
{
  osEvent evt;
  GPIO_InitTypeDef GPIO_InitStruct;

  FATFS SDFatFs;
  FIL logFile;
  FRESULT res;

  /* Reset microSD, set all pins to LOW to disconnect all power paths. Reinit is done in MX_SDMMC1_SD_Init() */
  SD_POWEROFF();
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = GPIO_PIN_2;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  HAL_Delay(200);
  SD_POWERON();

  /* Init microSD */
  MX_SDMMC1_SD_Init();
  hsd1.Init.HardwareFlowControl = SDMMC_HARDWARE_FLOW_CONTROL_ENABLE; //Very important, otherwise frequent RX_OVERFLOW errors
  hsd1.Init.ClockBypass = SDMMC_CLOCK_BYPASS_ENABLE; //No clock divider, microSD-clock is CLK48 with 48MHz

  /* Init fatFS */
  MX_FATFS_Init();
  if(HAL_SD_Init(&hsd1) != HAL_OK) {
    /* SD can't be initialised. Possible Errors: sd not present, sd faulty, hardware faulty (connection break) */
    printf("SD Init Error: 0x%lx\n", hsd1.ErrorCode);
    HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
    osDelay(300);
    HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
  }
  else {
    /* Test if we can write a file to microSD and if reading it out is ok */
    res = diskWriteTest();
    if(res != FR_OK) {
      /* Error while writing/reading. Possible Errors: sd not correctly formatted */
      printf("diskWriteTest Error: %i\n", res);
      HAL_GPIO_WritePin(LED_Y_GPIO_Port, LED_Y_Pin, GPIO_PIN_SET);
      HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
      osDelay(300);
      HAL_GPIO_WritePin(LED_Y_GPIO_Port, LED_Y_Pin, GPIO_PIN_RESET);
      HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
    }
    else {
      /* Read configuration .ini */
      readConfiguration();

      HAL_RTC_SetDate(&hrtc, &(g_config.date), RTC_FORMAT_BIN);
      HAL_RTC_SetTime(&hrtc, &(g_config.time), RTC_FORMAT_BIN);
      HAL_RTC_WaitForSynchro(&hrtc);

      /* Write card info to text file on card */
      if(g_config.dev) {
        if(writeSdInfo() != 0) {
          printf("writeSdInfo Error");
        }
      }

      /* Write unique ID to text file on card */
      writeUid();

      g_sdPresent = 1;

      MX_USB_DEVICE_Init();
    }
  }

  /* Fill rmsBuffer with zeroes */
  float *rmsTmp = rmsBuffer;
  while(rmsTmp < rmsBuffer + RMSBUFSIZE) {
    *(rmsTmp++) = 0.0f;
  }

  osSignalSet(vibrationTaskHandle, SIG_SDREADY);

  rawAdcData_t *pd;
  uint32_t overflows;
  UINT byteswritten;

  float writeBuffer[ADC2SAMPLECHCNT*ADC2BUFCNT/2];
  float *curWriteBuffer;
  uint16_t *curAdcSample;

  uint32_t visuCounter = VISU_DEVIDER;

  float *rmsHead = rmsBuffer;
  float *rmsTail = rmsBuffer+1;
  float u;
  float y = 0;
  float rmsMin, rmsMax, rmsAvg;
  float tempCelsius = 0;


  while(1) {
    evt = osMailGet(q_rawAdcDataHandle, osWaitForever);
    if(evt.status == osEventMail) {
      pd = (rawAdcData_t *)evt.value.p;
      overflows = pd->overflows;

      /* Process data */
      rmsMin = 0;
      rmsMax = 0;
      rmsAvg = 0;

      curWriteBuffer = writeBuffer;
      curAdcSample = pd->data;
      for(int s = 0; s < ADC2BUFCNT/2; s++) {
        for(int logCh = 0; logCh < g_config.nLogChannels; logCh++) {
          int selCh = g_config.logChannelOrder[logCh];
          *(curWriteBuffer++) = ((float)(curAdcSample[selCh]) / adcCal[selCh].hardSens - adcCal[selCh].hardOffset) / adcCal[selCh].softSens - adcCal[selCh].softOffset;
        }

        u = ((float)(curAdcSample[MEMS_Z]) / adcCal[MEMS_Z].hardSens - adcCal[MEMS_Z].hardOffset) / adcCal[MEMS_Z].softSens - adcCal[MEMS_Z].softOffset;

        curAdcSample += ADC2SAMPLECHCNT;

        /* Calculate moving RMS */
        y = sqrtf( powf(y, 2) + 1.0f * (1.0f/(float)MOVINGRMSWINDOW) * ( powf(u, 2) - powf(*rmsTail, 2) ) );

        /* Due to floating point inaccuracy the radicant can become (a very small) negative value. sqrtf then returns nan.
         * If y is nan it should be a very small number equal or near zero. As we don't know the real value, set it to zero.
         */
        if(isnanf(y)) y = 0;

        /* RMS min, max, avg */
        rmsMin = (y < rmsMin) ? y : rmsMin;
        rmsMax = (y > rmsMax) ? y : rmsMax;
        rmsAvg += y;

        /* Debug output of RMS */
        //*(curWriteBuffer - 3) = y;

        /* Move head and tail */
        if(++rmsHead >= rmsBuffer + RMSBUFSIZE) rmsHead = rmsBuffer;
        if(++rmsTail >= rmsBuffer + RMSBUFSIZE) rmsTail = rmsBuffer;

        *rmsHead = u;
      }

      /* For visu */
      curAdcSample -= ADC2SAMPLECHCNT; // undo increment
      tempCelsius = ((float)(curAdcSample[RTD]) / adcCal[RTD].hardSens - adcCal[RTD].hardOffset) / adcCal[RTD].softSens - adcCal[RTD].softOffset;

      osMailFree(q_rawAdcDataHandle, evt.value.p);

      /* Divide Average */
      rmsAvg = rmsAvg / (float)(ADC2BUFCNT/2);

      /* Visu data over UART to bluetooth */
      if(--visuCounter == 0) {
        uint8_t visuBuffer[3*sizeof(float)];

        g_modbusData.sensor[0] = rmsAvg;
        g_modbusData.sensor[1] = tempCelsius;


        *( ((float *)visuBuffer)+0 ) = 262144.0f; //2^18 = 262144.0
        *( ((float *)visuBuffer)+1 ) = g_modbusData.sensor[0];
        *( ((float *)visuBuffer)+2 ) = g_modbusData.sensor[1];

        //while(!LL_USART_IsActiveFlag_TC(USART1));
        //LL_USART_TransmitData8(USART1, 'a');

        visuCounter = VISU_DEVIDER;
      }

      switch(g_recState) {
        case STATE_REC_STARTING:
          if(g_config.enableLog) {
            USBD_Stop(&hUsbDeviceFS);

            if(f_mount(&SDFatFs, (TCHAR const*)SD_Path, 0) != FR_OK) Error_Handler();

            char filename[15];
            uint32_t counter = 0;
            do {
              snprintf(filename, sizeof(filename), "log%04lu.bin", counter++);
              res = f_open(&logFile, filename, FA_CREATE_NEW | FA_WRITE);
            } while(res == FR_EXIST && counter <= 9999);
            if(res != FR_OK) Error_Handler();
          }

          HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_SET);
          g_modbusData.control[MB_REG_CONTROL_RECSTATUS] = 1;

          g_recState = STATE_REC_RUNNING;
          //No break, continue to RUNNING
        case STATE_REC_RUNNING:
          if(overflows > 0) {
            //printf("Leaving early, overflows: %lu\n", overflows);
            g_recState = STATE_REC_STOPPING;
          }
          else {
            /* Write data to SD */
            HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);

            if(g_config.enableLog) {
              res = f_write(&logFile, writeBuffer, g_config.nLogChannels*ADC2BUFCNT/2*sizeof(writeBuffer[0]), (void *)&byteswritten);
              if(res != FR_OK) Error_Handler();
              res = f_sync(&logFile);
              if(res != FR_OK) Error_Handler();
            }
            else {
              HAL_Delay(1);
            }

            HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_SET);
            break; //Break only if write was successful, else stop
          }
          //No break
        case STATE_REC_STOPPING:
          if(g_config.enableLog) {
            if(f_close(&logFile) != FR_OK) Error_Handler();
            if(f_mount(NULL, (TCHAR const*)SD_Path, 1) != FR_OK) Error_Handler();
            USBD_Start(&hUsbDeviceFS);
          }
          HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);
          g_modbusData.control[MB_REG_CONTROL_RECSTATUS] = 0;

          g_recState = STATE_REC_STOPPED;
          //No break, continue to STOPPED
        case STATE_REC_STOPPED:
          break;
      }
    }
  }
}

/*
 * hex:
 * [30000]: 0x0304
 * [30001]: 0x0102
 * [30002]: 0x2000
 * [30003]: 0x47F1
 *
 * int32:
 * [30000]: 16909060
 * [30002]: 1206984704
 *
 * float32:
 * [30000]: 0.000000
 * [30002]: 123456.000000
 */


void StartBtTask(void const * argument)
{
  snprintf(g_modbusData.cmsSerialNumber, sizeof(g_modbusData.cmsSerialNumber), "%08lX-%08lX-%08lX",
    *(((uint32_t*)UID_BASE)+0),
    *(((uint32_t*)UID_BASE)+1),
    *(((uint32_t*)UID_BASE)+2)
  );
  snprintf(g_modbusData.cmsSoftwareVersion, sizeof(g_modbusData.cmsSoftwareVersion), "%s",
    gitDescribe
  );
  snprintf(g_modbusData.cmsDeviceVersion, sizeof(g_modbusData.cmsDeviceVersion), "%s",
    "GAT CMS v1"
  );

  /* Customer data */
  g_modbusData.customerData[0] = 0;
  g_modbusData.customerData[1] = 0;

  /* Vendor data */
  snprintf(g_modbusData.vendorPartNumber, sizeof(g_modbusData.vendorPartNumber), "%s",
    "159532026142SP"
  );
  snprintf(g_modbusData.vendorPartName, sizeof(g_modbusData.vendorPartName), "%s",
    "ROTODISK S0-AKM-CM"
  );
  snprintf(g_modbusData.vendorSerialNumber, sizeof(g_modbusData.vendorSerialNumber), "%s",
    "301644"
  );
  snprintf(g_modbusData.vendorMedia1, sizeof(g_modbusData.vendorMedia1), "%s",
    "coolant"
  );
  snprintf(g_modbusData.vendorMedia2, sizeof(g_modbusData.vendorMedia2), "%s",
    "MQL"
  );
  snprintf(g_modbusData.vendorMedia3, sizeof(g_modbusData.vendorMedia3), "%s",
    "air"
  );
  g_modbusData.vendorDeliveryDate = 1536098400;
  g_modbusData.vendorMaxPressure1 = 140;
  g_modbusData.vendorMaxPressure2 = 10;
  g_modbusData.vendorMaxPressure3 = 10;
  g_modbusData.vendorMaxSpeed = 28000;

  eMBInit( MB_RTU, 1, 1, 115200, MB_PAR_NONE );
  eMBEnable();

  while(1) {
    eMBPoll();
  }
}

void StartThresholdTask(void const * argument)
{
  uint32_t previousWakeTime = osKernelSysTick();

  /* Reset all values */
  for(int i = 0; i < N_SENSORS; i++) {
    g_modbusData.sensor[i] = 0.0;
    g_modbusData.thr1[i] = 0.0;
    g_modbusData.thr2[i] = 0.0;
    g_modbusData.thr3[i] = 0.0;
    g_modbusData.thrCnt1[i] = 0;
    g_modbusData.thrCnt2[i] = 0;
    g_modbusData.thrCnt3[i] = 0;
    g_modbusData.cmParam[i] = 0.0;
    g_modbusData.cmParam[i+10] = 0.0;
    g_modbusData.cmParam[i+20] = 0.0;
  }

  while(1) {
    float cmIndicator = 0;
    float cmIndicatorDiff = 0;
    float cmIndicatorReference = 0;
    float cmIndicatorRatio = 0;

    /* Calculate condition monitoring indicator and increment threshold counters */
    for(int i = 0; i < N_SENSORS; i++) {
      if(g_modbusData.sensor[i] > g_modbusData.thr3[i]) {
        if(g_modbusData.thrCnt3[i] < 0xFFFF) g_modbusData.thrCnt3[i]++;
        cmIndicatorDiff += g_modbusData.cmParam[2*10 + i];
      }
      else if(g_modbusData.sensor[i] > g_modbusData.thr2[i]) {
        if(g_modbusData.thrCnt2[i] < 0xFFFF) g_modbusData.thrCnt2[i]++;
        cmIndicatorDiff += g_modbusData.cmParam[1*10 + i];
      }
      else if(g_modbusData.sensor[i] > g_modbusData.thr1[i]) {
        if(g_modbusData.thrCnt1[i] < 0xFFFF) g_modbusData.thrCnt1[i]++;
        cmIndicatorDiff += g_modbusData.cmParam[0*10 + i];
      }
    }
    g_modbusData.sensor[MB_SENS_CM] += cmIndicatorDiff;

    /* Calculate condition monitoring indicator ratio */
    cmIndicator = g_modbusData.sensor[MB_SENS_CM];
    cmIndicatorReference = g_modbusData.sensor[MB_SENS_CMREF];

    if(isnanf(cmIndicator) || isinff(cmIndicator)) cmIndicator = 0;
    if(cmIndicatorReference != 0 && !isnanf(cmIndicatorReference) && !isinff(cmIndicatorReference)) cmIndicatorRatio = cmIndicator / cmIndicatorReference;
    if(isnanf(cmIndicatorRatio) || isinff(cmIndicatorRatio)) cmIndicatorRatio = 0;

    g_modbusData.sensor[MB_SENS_CMRATIO] = cmIndicatorRatio;

    /* Do calculation every second */
    osDelayUntil(&previousWakeTime, 1000);
  }
}


WCHAR ff_convert (WCHAR wch, UINT dir)
{
          if (wch < 0x80) {
                    /* ASCII Char */
                    return wch;
          }

          /* I don't support unicode it is too big! */
          return 0;
}

WCHAR ff_wtoupper (WCHAR wch)
{
          if (wch < 0x80) {
                    /* ASCII Char */
                    if (wch >= 'a' && wch <= 'z') {
                              wch &= ~0x20;
                     }
                      return wch;
          }

          /* I don't support unicode it is too big! */
          return 0;
}

int _write(int file, char *data, int len)
{
   if ((file != STDOUT_FILENO) && (file != STDERR_FILENO))
   {
      errno = EBADF;
      return -1;
   }

   // arbitrary timeout 1000
   //HAL_StatusTypeDef status = HAL_UART_Transmit(&huart1, (uint8_t*)data, len, 1000);
   //return (status == HAL_OK ? len : 0);
   return len;
   //return CDC_Transmit_FS((uint8_t*)data, len) == USBD_OK;

   // return # of bytes written - as best we can tell

}

FRESULT diskWriteTest()
{
  FATFS SDFatFs;
  FIL testfile;
  FRESULT res;

  UINT byteswritten, bytesread;
  char wtext[5] = "test";
  char rtext[5];
  const char testFileName[] = "a8s4f8e";

  res = f_mount(&SDFatFs, (TCHAR const*)SD_Path, 1);
  if(res != FR_OK) return res;

  res = f_open(&testfile, testFileName, FA_CREATE_ALWAYS | FA_WRITE);
  if(res != FR_OK) return res;
  res = f_write(&testfile, wtext, sizeof(wtext), (void *)&byteswritten);
  if(res != FR_OK) return res;
  res = f_close(&testfile);
  if(res != FR_OK) return res;

  res = f_open(&testfile, testFileName, FA_READ);
  if(res != FR_OK) return res;
  res = f_read(&testfile, rtext, sizeof(rtext), &bytesread);
  if(res != FR_OK) return res;
  res = f_close(&testfile);
  if(res != FR_OK) return res;

  f_unlink(testFileName);
  f_mount(NULL, (TCHAR const*)SD_Path, 1);

  if(strncmp(wtext, rtext, sizeof(wtext)) != 0) return FR_INT_ERR;

  return FR_OK;
}

int writeSdInfo()
{

  FATFS SDFatFs;
  FIL infoFile;
  FRESULT res;

  char infoFileName[50];

  HAL_SD_CardCIDTypedef cid;
  HAL_SD_CardCSDTypedef csd;
  HAL_SD_CardInfoTypeDef cardinfo;

  if(HAL_SD_GetCardCID(&hsd1, &cid) != HAL_OK) return 1;
  if(HAL_SD_GetCardCSD(&hsd1, &csd) != HAL_OK) return 1;
  if(HAL_SD_GetCardInfo(&hsd1, &cardinfo) != HAL_OK) return 1;

  res = f_mount(&SDFatFs, (TCHAR const*)SD_Path, 0);
  if(res != FR_OK) return 1;

  uint8_t ManufacturerID = cid.ManufacturerID;
  char OEM_AppliID[3] = {(char)(cid.OEM_AppliID>>8), (char)(cid.OEM_AppliID), '\0'};
  char ProdName[6] = {(char)(cid.ProdName1>>24), (char)(cid.ProdName1>>16), (char)(cid.ProdName1>>8), (char)(cid.ProdName1>>0), (char)(cid.ProdName2>>0), '\0'};
  uint8_t hwrev = cid.ProdRev>>4;
  uint8_t swrev = cid.ProdRev&0x0f;
  uint32_t ProdSN = cid.ProdSN;
  uint32_t dateYY = (cid.ManufactDate>>8)*10 + ((cid.ManufactDate>>4)&0x0f) + 2000;
  uint32_t dateM = ((cid.ManufactDate)&0x00f);
  uint32_t sizeGB = (((cardinfo.BlockNbr/1000ul)*cardinfo.BlockSize)/1000ul + 500ul)/1000ul;

  snprintf(infoFileName, sizeof(infoFileName)/sizeof(char), "%luGB_%u-%s-%s-%u_%u-%lu_%lu-%lu.txt", sizeGB, ManufacturerID, OEM_AppliID, ProdName, hwrev, swrev, dateYY, dateM, ProdSN);

  if(f_open(&infoFile, infoFileName, FA_CREATE_ALWAYS | FA_WRITE) != FR_OK) return 1;
  f_printf(&infoFile, "--- CardInfo ---\r\n");
  f_printf(&infoFile, "CardType: 0x%x\r\n"
    "CardVersion: 0x%x\r\n"
    "Class: 0x%x\r\n"
    "BlockNbr: %u\r\n"
    "BlockSize: %u\r\n"
    "LogBlockNbr: %u\r\n"
    "LogBlockSize: %u\r\n"
    "RelCardAdd: 0x%x\r\n",
    cardinfo.CardType, cardinfo.CardVersion, cardinfo.Class, cardinfo.BlockNbr, cardinfo.BlockSize, cardinfo.LogBlockNbr,
    cardinfo.LogBlockSize, cardinfo.RelCardAdd);

  f_printf(&infoFile, "\r\n--- CID ---\r\n");
  f_printf(&infoFile, "ManufacturerID: 0x%x\r\n"
    "OEM_AppliID: 0x%x\r\n"
    "ProdName1: 0x%x\r\n"
    "ProdName2: 0x%x\r\n"
    "ProdRev: 0x%x\r\n"
    "ProdSN: 0x%x\r\n"
    "ManufactDate: 0x%x\r\n",
    cid.ManufacturerID, cid.OEM_AppliID, cid.ProdName1, cid.ProdName2, cid.ProdRev, cid.ProdSN, cid.ManufactDate);

  f_printf(&infoFile, "\r\n--- CSD ---\r\n");
  f_printf(&infoFile, "CSDStruct: 0x%x\r\n"
    "SysSpecVersion: 0x%x\r\n"
    "Reserved1: 0x%x\r\n"
    "TAAC: 0x%x\r\n"
    "NSAC: 0x%x\r\n"
    "MaxBusClkFrec: 0x%x\r\n"
    "CardComdClasses: 0x%x\r\n"
    "RdBlockLen: 0x%x\r\n"
    "PartBlockRead: 0x%x\r\n"
    "WrBlockMisalign: 0x%x\r\n"
    "RdBlockMisalign: 0x%x\r\n"
    "DSRImpl: 0x%x\r\n"
    "Reserved2: 0x%x\r\n"
    "DeviceSize: 0x%x\r\n"
    "MaxRdCurrentVDDMin: 0x%x\r\n"
    "MaxRdCurrentVDDMax: 0x%x\r\n"
    "MaxWrCurrentVDDMin: 0x%x\r\n"
    "MaxWrCurrentVDDMax: 0x%x\r\n"
    "DeviceSizeMul: 0x%x\r\n"
    "EraseGrSize: 0x%x\r\n"
    "EraseGrMul: 0x%x\r\n"
    "WrProtectGrSize: 0x%x\r\n"
    "WrProtectGrEnable: 0x%x\r\n"
    "ManDeflECC: 0x%x\r\n"
    "WrSpeedFact: 0x%x\r\n"
    "MaxWrBlockLen: 0x%x\r\n"
    "WriteBlockPaPartial: 0x%x\r\n"
    "Reserved3: 0x%x\r\n"
    "ContentProtectAppli: 0x%x\r\n"
    "FileFormatGrouop: 0x%x\r\n"
    "CopyFlag: 0x%x\r\n"
    "PermWrProtect: 0x%x\r\n"
    "TempWrProtect: 0x%x\r\n"
    "FileFormat: 0x%x\r\n"
    "ECC: 0x%x\r\n"
    "CRC: 0x%x\r\n"
    "Reserved4: 0x%x\r\n",
    csd.CSDStruct, csd.SysSpecVersion, csd.Reserved1, csd.TAAC, csd.NSAC, csd.MaxBusClkFrec, csd.CardComdClasses,
    csd.RdBlockLen, csd.PartBlockRead, csd.WrBlockMisalign, csd.RdBlockMisalign, csd.DSRImpl, csd.Reserved2, csd.DeviceSize, csd.MaxRdCurrentVDDMin,
    csd.MaxRdCurrentVDDMax, csd.MaxWrCurrentVDDMin, csd.MaxWrCurrentVDDMax, csd.DeviceSizeMul, csd.EraseGrSize, csd.EraseGrMul, csd.WrProtectGrSize,
    csd.WrProtectGrEnable, csd.ManDeflECC, csd.WrSpeedFact, csd.MaxWrBlockLen, csd.WriteBlockPaPartial, csd.Reserved3, csd.ContentProtectAppli,
    csd.FileFormatGrouop, csd.CopyFlag, csd.PermWrProtect, csd.TempWrProtect, csd.FileFormat, csd.ECC, csd.CSD_CRC, csd.Reserved4);

   if(f_close(&infoFile) != FR_OK) return 1;
   if(f_mount(NULL, (TCHAR const*)SD_Path, 1) != FR_OK) return 1;

   return 0;
}

void writeUid()
{
  FATFS SDFatFs;
  FIL uidFile;
  FRESULT res;

  res = f_mount(&SDFatFs, (TCHAR const*)SD_Path, 0);
  if(res != FR_OK) return;

  if(f_open(&uidFile, "uid.txt", FA_CREATE_ALWAYS | FA_WRITE) != FR_OK) return;
  f_printf(&uidFile, "gat-cms_%08X-%08X-%08X\r\n%s\r\n",
    *(((uint32_t*)UID_BASE)+0),
    *(((uint32_t*)UID_BASE)+1),
    *(((uint32_t*)UID_BASE)+2),
    gitDescribe
  );
  if(f_close(&uidFile) != FR_OK) return;
  if(f_mount(NULL, (TCHAR const*)SD_Path, 1) != FR_OK) return;
}

int ini_parse_fatfs(const char* filename, ini_handler handler, void* user)
{
  FRESULT result;
  FIL file;
  int error;

  result = f_open(&file, filename, FA_OPEN_EXISTING | FA_READ);
  if (result != FR_OK)
    return -1;
  error = ini_parse_stream((ini_reader)f_gets, &file, handler, user);
  f_close(&file);
  return error;
}


static int handler(void* user, const char* section, const char* name,
                   const char* value)
{
    globalConfig_t *config = (globalConfig_t*)user;
    //printf("%s, %s: %s\n", section, name, value);

    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
    #define CHECKRANGE(v, min, max) (v >= min && v <= max)

    if(MATCH("time", "year") && CHECKRANGE(atoi(value), 0, 99)) config->date.Year = atoi(value);
    else if(MATCH("time", "month") && CHECKRANGE(atoi(value), 1, 12)) config->date.Month = atoi(value) - 1;
    else if(MATCH("time", "day") && CHECKRANGE(atoi(value), 1, 31)) config->date.Date = atoi(value);
    else if(MATCH("time", "hours") && CHECKRANGE(atoi(value), 0, 23)) config->time.Hours = atoi(value);
    else if(MATCH("time", "minutes") && CHECKRANGE(atoi(value), 0, 59)) config->time.Minutes = atoi(value);

    if(MATCH("settings", "enablelog")) config->enableLog = (strcmp(value, "false") == 0) ? 0 : 1;
    else if(MATCH("settings", "dev")) config->dev = (strcmp(value, "true") == 0) ? 1 : 0;
    else if(MATCH("settings", "channelcount")) {
      if(atoi(value) > 0) {
        if(atoi(value) <= ADC2DEFAULTLOGCHCNT) config->nLogChannels = atoi(value);
        else if(atoi(value) <= ADC2SAMPLECHCNT && config->dev) config->nLogChannels = atoi(value);
      }
    }

    else if(strcmp(section, "channels") == 0) {
      if(atoi(value) >= 0 && atoi(value) < ADC2SAMPLECHCNT) {
        if(strcmp(name, "logch0") == 0) config->logChannelOrder[0] = atoi(value);
        else if(strcmp(name, "logch1") == 0) config->logChannelOrder[1] = atoi(value);
        else if(strcmp(name, "logch2") == 0) config->logChannelOrder[2] = atoi(value);
        else if(strcmp(name, "logch3") == 0) config->logChannelOrder[3] = atoi(value);
        else if(strcmp(name, "logch4") == 0) config->logChannelOrder[4] = atoi(value);
        else if(strcmp(name, "logch5") == 0) config->logChannelOrder[5] = atoi(value);
      }
    }
    else if (!isnanf(atof(value)) && !isinf(atof(value)) ) {
      if(atof(value) > 0) {
        if(MATCH("mems_x", "sensitivity"))  config->adcCal[MEMS_X].softSens = atof(value);
        else if(MATCH("mems_y", "sensitivity"))  config->adcCal[MEMS_Y].softSens = atof(value);
        else if(MATCH("mems_z", "sensitivity"))  config->adcCal[MEMS_Z].softSens = atof(value);
        else if(MATCH("pzo", "sensitivity"))  config->adcCal[PZO].softSens = atof(value);
        else if(MATCH("pzo_ext", "sensitivity"))  config->adcCal[PZO_EXT].softSens = atof(value);
        else if(MATCH("rtd", "sensitivity"))  config->adcCal[RTD].softSens = atof(value);
      }
      if(MATCH("mems_x", "offset"))  config->adcCal[MEMS_X].softOffset = atof(value);
      else if(MATCH("mems_y", "offset"))  config->adcCal[MEMS_Y].softOffset = atof(value);
      else if(MATCH("mems_z", "offset"))  config->adcCal[MEMS_Z].softOffset = atof(value);
      else if(MATCH("pzo", "offset"))  config->adcCal[PZO].softOffset = atof(value);
      else if(MATCH("pzo_ext", "offset"))  config->adcCal[PZO_EXT].softOffset = atof(value);
      else if(MATCH("rtd", "offset"))  config->adcCal[RTD].softOffset = atof(value);
    }
    else {
      return 0;  /* unknown section/name, error */
    }
    return 1;
}

int readConfiguration()
{

  FATFS SDFatFs;
  FRESULT res;

  res = f_mount(&SDFatFs, (TCHAR const*)SD_Path, 0);
  if(res != FR_OK) return 1;

  if (ini_parse_fatfs("config.ini", handler, &g_config) < 0) {
    //printf("Can't load 'test.ini'\n");
    f_mount(NULL, (TCHAR const*)SD_Path, 1);
    return 1;
  }
  f_mount(NULL, (TCHAR const*)SD_Path, 1);

  return 0;

}


void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc)
{
  if(hadc == &hadc2) {
    //LED_Y_GPIO_Port->BSRR = LED_Y_Pin;
    osSignalSet(vibrationTaskHandle, SIG_ADC2HALF);
  }

}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
  if(hadc == &hadc2) {
    //LED_Y_GPIO_Port->BSRR = LED_Y_Pin;
    osSignalSet(vibrationTaskHandle, SIG_ADC2CPLT);
  }
}
/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
/* USER CODE BEGIN Callback 0 */

/* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
/* USER CODE BEGIN Callback 1 */
  if (htim->Instance == TIM7) {
    pxMBPortCBTimerExpired();
  }
/* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  HAL_GPIO_WritePin(LED_0_GPIO_Port, LED_0_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);

  printf("Error in %s:%i\n", file, line);
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
