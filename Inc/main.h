/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* USER CODE BEGIN Includes */
#define NDEBUG
/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define MEMS_TEMP_Pin GPIO_PIN_3
#define MEMS_TEMP_GPIO_Port GPIOC
#define PIEZO_EXT_Z_Pin GPIO_PIN_2
#define PIEZO_EXT_Z_GPIO_Port GPIOA
#define MEMS_Y_Pin GPIO_PIN_4
#define MEMS_Y_GPIO_Port GPIOA
#define MEMS_X_Pin GPIO_PIN_5
#define MEMS_X_GPIO_Port GPIOA
#define PIEZO_Z_Pin GPIO_PIN_6
#define PIEZO_Z_GPIO_Port GPIOA
#define RTD_Pin GPIO_PIN_4
#define RTD_GPIO_Port GPIOC
#define MEMS_STBY_Pin GPIO_PIN_5
#define MEMS_STBY_GPIO_Port GPIOC
#define MEMS_ST2_Pin GPIO_PIN_2
#define MEMS_ST2_GPIO_Port GPIOB
#define MEMS_ST1_Pin GPIO_PIN_10
#define MEMS_ST1_GPIO_Port GPIOB
#define MEMS_RANGE_Pin GPIO_PIN_11
#define MEMS_RANGE_GPIO_Port GPIOB
#define LED_R_Pin GPIO_PIN_12
#define LED_R_GPIO_Port GPIOB
#define LED_Y_Pin GPIO_PIN_13
#define LED_Y_GPIO_Port GPIOB
#define LED_G_Pin GPIO_PIN_14
#define LED_G_GPIO_Port GPIOB
#define LED_0_Pin GPIO_PIN_15
#define LED_0_GPIO_Port GPIOB
#define SD_EN_Pin GPIO_PIN_15
#define SD_EN_GPIO_Port GPIOA
#define DO2_Pin GPIO_PIN_3
#define DO2_GPIO_Port GPIOB
#define DO1_Pin GPIO_PIN_4
#define DO1_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */
#define MEMS_AMP (5.0f/3.0f)
//#define ADCTOVOLT (3.0f/4095.0f)
#define LSBPERVOLT (4095.0f/3.0f)

//MEMS
#define OFFSET_MEMS_X (1.8f/2.0f)
#define SENS_MEMS_X (0.08f) // V/g
#define OFFSET_MEMS_Y (1.8f/2.0f)
#define SENS_MEMS_Y (0.08f) // V/g
#define OFFSET_MEMS_Z (1.8f/2.0f * MEMS_AMP)
#define SENS_MEMS_Z (0.08f * MEMS_AMP) // V/g

//PZO
#define OFFSET_PZO (1.5f)
#define SENS_PZO (-0.1f) // V/g

//RTD
#define OFFSET_RTD (0.0f) // V
#define SENS_RTD (0.018f) // V/Ohm
#define OFFSET_TEMP (258.0f) // �C
#define SENS_TEMP (0.388f) // Ohm/�C



#define MEMS_RANGE_10G()        HAL_GPIO_WritePin(MEMS_RANGE_GPIO_Port, MEMS_RANGE_Pin, GPIO_PIN_RESET)
#define MEMS_RANGE_2040G()      HAL_GPIO_WritePin(MEMS_RANGE_GPIO_Port, MEMS_RANGE_Pin, GPIO_PIN_SET)
#define MEMS_STANDBY_ENABLE()   HAL_GPIO_WritePin(MEMS_STBY_GPIO_Port, MEMS_STBY_Pin, GPIO_PIN_RESET)
#define MEMS_STANDBY_DISABLE()  HAL_GPIO_WritePin(MEMS_STBY_GPIO_Port, MEMS_STBY_Pin, GPIO_PIN_SET)

#define SD_POWERON()            HAL_GPIO_WritePin(SD_EN_GPIO_Port, SD_EN_Pin, GPIO_PIN_RESET)
#define SD_POWEROFF()           HAL_GPIO_WritePin(SD_EN_GPIO_Port, SD_EN_Pin, GPIO_PIN_SET)

#define VISU_DEVIDER    (10u) // min. 1!

//#define ADC1CHCNT       3
#define ADC2SAMPLECHCNT       6
#define ADC2DEFAULTLOGCHCNT       3
//#define ADC1BUFCNT      512*2
#define ADC2BUFCNT      512*2
//#define ADC1BUFSIZE     ADC1CHCNT*ADC1BUFCNT
#define ADC2BUFSIZE     ADC2SAMPLECHCNT*ADC2BUFCNT

#define MOVINGRMSWINDOW 10000
#define RMSBUFCNT       MOVINGRMSWINDOW
#define RMSBUFSIZE      RMSBUFCNT

#define SIG_ADC1HALF    1<<0
#define SIG_ADC1CPLT    1<<1
#define SIG_ADC2HALF    1<<2
#define SIG_ADC2CPLT    1<<3
#define SIG_SDREADY   1<<4

#define SIG_D1A    1<<0
#define SIG_D1B    1<<1
#define SIG_D2A    1<<2
#define SIG_D2B    1<<3

enum recordingState {
  STATE_REC_STOPPED = 0,
  STATE_REC_STOPPING,
  STATE_REC_STARTING,
  STATE_REC_RUNNING
};

/* USER CODE END Private defines */

void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
