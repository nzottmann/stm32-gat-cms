/*
 * modbus.h
 *
 *  Created on: 14.05.2018
 *      Author: N
 */

#ifndef MODBUS_H_
#define MODBUS_H_

#include "mb.h"
#include "mbport.h"

#define N_SENSORS 10
#define N_CM_PARAM N_SENSORS*3
#define N_CONTROL_PARAM N_SENSORS*3

#define MB_SENS_VIBRMSMEMSZ 0
#define MB_SENS_TEMPBEARING 1
#define MB_SENS_CM 5
#define MB_SENS_CMREF 6
#define MB_SENS_CMRATIO 7

#define MB_INRANGE(checkAddress, checkNRegs, address, nRegs) ( (checkAddress >= address) && ((checkAddress + checkNRegs) <= (address + nRegs)) )

#define MB_REG_SENSOR_START 0
#define MB_REG_SENSOR_NREGS 20
#define MB_REG_THR1_START 100
#define MB_REG_THR1_NREGS 20
#define MB_REG_THR2_START 200
#define MB_REG_THR2_NREGS 20
#define MB_REG_THR3_START 300
#define MB_REG_THR3_NREGS 20
#define MB_REG_THRCNT1_START 400
#define MB_REG_THRCNT1_NREGS 10
#define MB_REG_THRCNT2_START 500
#define MB_REG_THRCNT2_NREGS 10
#define MB_REG_THRCNT3_START 600
#define MB_REG_THRCNT3_NREGS 10
#define MB_REG_CM_START 700
#define MB_REG_CM_NREGS 60
#define MB_REG_CUSTOMER_START 2000
#define MB_REG_CUSTOMER_NREGS 4
#define MB_REG_CONTROL_START 7000
#define MB_REG_CONTROL_NREGS 2
#define MB_REG_INFO_START 9000
#define MB_REG_INFO_NREGS 135
#define MB_REG_INFONUMERIC_START 9135
#define MB_REG_INFONUMERIC_NREGS 10

#define MB_REG_CONTROL_RECSTATUS 0
#define MB_REG_CONTROL_RECCONTROL 1


typedef struct {
  float sensor[N_SENSORS];
  float thr1[N_SENSORS];
  float thr2[N_SENSORS];
  float thr3[N_SENSORS];
  uint16_t thrCnt1[N_SENSORS];
  uint16_t thrCnt2[N_SENSORS];
  uint16_t thrCnt3[N_SENSORS];
  float cmParam[N_SENSORS*3];
  uint32_t customerData[2];
  uint16_t control[N_CONTROL_PARAM];
  char cmsSerialNumber[30];
  char cmsDeviceVersion[30];
  char cmsSoftwareVersion[30];
  char vendorPartNumber[30];
  char vendorPartName[30];
  char vendorSerialNumber[30];
  char vendorMedia1[30];
  char vendorMedia2[30];
  char vendorMedia3[30];
  uint32_t vendorDeliveryDate;
  uint32_t vendorMaxPressure1;
  uint32_t vendorMaxPressure2;
  uint32_t vendorMaxPressure3;
  uint32_t vendorMaxSpeed;
} __attribute__ ((packed)) modbusData_t;

typedef struct {
  USHORT regStart;
  USHORT nRegs;
  USHORT *buffer;
  BOOL byteSwap;
  void(*readCallback)(void);
  void(*writeCallback)(void);
} modbusMemory_t;

modbusData_t g_modbusData;

extern enum recordingState g_recState;
extern uint32_t g_sdPresent;

#endif /* MODBUS_H_ */
